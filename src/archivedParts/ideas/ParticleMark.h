namespace TNL {
namespace ParticleSystem {
namespace ParticleMark {

template< typename Device >
class None
{
public:

   void init() {};

};

template< typename Device >
class UseWithParticleMark
{
public:

};

} //namespace ParticleMark
} //namespace Particles
} //namespace TNL

