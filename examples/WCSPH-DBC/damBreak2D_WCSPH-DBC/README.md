## 2D dam break example

Two-dimensional implementation of the standard SPH example, compared with experimental data provided in following publication:

- L. Lobovsky et al. [Experimental investigation of dynamic pressure loads during dam break](https://doi.org/10.1016/j.jfluidstructs.2014.03.009). In: Journal of Fluids and Structures 48 (Aug. 2013).

#### Scheme of the example

![dmBreak2D_scheme](../../resources/damBreak2D/damBreak_scheme.png "2D dam break example scheme")
